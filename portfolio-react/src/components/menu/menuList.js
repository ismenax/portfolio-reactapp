import { Link } from "react-router-dom";
import imgHome from '../../img/sygnet_kwadraty_.png';
import imgAbout from '../../img/ikona_about_me.png';
import imgPortfolio from '../../img/ikona_portfolio.png';
import imgBlog from '../../img/ikona_blog.png';
import imgContact from '../../img/ikona_mailing_contact.png';
import imgLine from '../../img/_percentage_line_menu.png';
import imgBitbucket from '../../img/ikona_bitbucket.png';
import imgDEV from '../../img/ikona_DEV.png';
import imgTwitter from '../../img/twitter_icon.png';
import imgFacebook from '../../img/facebook_icon.png';
import imgLinkedIn from '../../img/linkedin_icon.png';

const MenuList = () => {
    const list =
    {
        menu: [
            {
                'id': 'home',
                'txt': 'Home',
                'img': imgHome,
                'route': './home',
                'className': 'main-nav__wrapper_top'
            },
            {
                'id': 'about',
                'txt': 'About',
                'img': imgAbout,
                'route': './about',
                'className': 'main-nav__wrapper_elem'
            },
            {
                'id': 'portfolio',
                'txt': 'Portfolio',
                'img': imgPortfolio,
                'route': './portfolio',
                'className': 'main-nav__wrapper_elem'
            },
            {
                'id': 'blog',
                'txt': 'Blog',
                'img': imgBlog,
                'route': './blog',
                'className': 'main-nav__wrapper_elem'
            },
            {
                'id': 'contact',
                'txt': 'Contact',
                'img': imgContact,
                'route': './contact',
                'className': 'main-nav__wrapper_elem'
            },
            {
                'id': 'line',
                'txt': '...',
                'img': imgLine,
                'route': '',
                'className': 'main-nav__wrapper_bar'
            },
            {
                'id': 'bitbucket',
                'txt': 'Bitbucket',
                'img': imgBitbucket,
                'route': './Bitbucket',
                'className': 'main-nav__wrapper_elem'
            },
            {
                'id': 'dev',
                'txt': 'DEV',
                'img': imgDEV,
                'route': './DEV',
                'className': 'main-nav__wrapper_elem'
            },
            {
                'id': 'twitter',
                'txt': 'Twitter',
                'img': imgTwitter,
                'route': './Twitter',
                'className': 'main-nav__wrapper_twitter'
            },
            {
                'id': 'facebook',
                'txt': 'Facebook',
                'img': imgFacebook,
                'route': './Facebook',
                'className': 'main-nav__wrapper_facebook'
            },
            {
                'id': 'linkedin',
                'txt': 'LinkedIn',
                'img': imgLinkedIn,
                'route': './LinkedIn',
                'className': 'main-nav__wrapper_linkedin'
            }
        ]
    }

    return (
        list.menu.map(item => {
            return (
                <li className={item.className} key={item.id} >
                    <Link to={item.route} ><img className="shadow" src={item.img} alt={item.txt} /></Link>
                </li>
            )
        })
    )
}
export default MenuList;