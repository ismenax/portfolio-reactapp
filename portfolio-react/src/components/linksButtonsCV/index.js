import imgButton from '../../img/button_01.png';
import { Link } from "react-router-dom";

const LinksButtonsCV = (props) => {
    return (
        <div className="cv-buttons">
            <div className="cv-buttons_button shadow">
                <Link to=""><img src={imgButton} alt="Download CV" /></Link>
                <div className="cv-buttons_button_button-text">Download&nbsp;CV</div>
            </div>
            <div className="cv-buttons_button shadow">
                <Link to=""><img src={imgButton} alt="Hire me" /></Link>
                <div className="cv-buttons_button_button-text">Hire&nbsp;me</div>
            </div>
        </div>
    )
}

export default LinksButtonsCV;