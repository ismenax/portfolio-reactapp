import React from "react";

const ArrowLink = (props) => {
    const text = props.text;
    return (
        <div className="blog-categories_list">
            {
                text.map((value, index) => {
                    return (
                        <React.Fragment key={index}>
                            <div className="blog-categories_list_elem">
                                <a href="/">
                                    <div className="blog-categories_list_elem_text">{value}</div>
                                    <div className="blog-categories_list_elem_sign">&gt;</div>
                                </a>
                            </div>
                        </React.Fragment>
                    )
                })
            }
        </div>
    )
}
export default ArrowLink;