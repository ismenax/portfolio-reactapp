import imgSelfie from '../../img/portfolio_programista_sidebar-1.png';
import imgSygnet from '../../img/sygnet_kwadraty_znacznik.png';

const Selfie = (props) => {
    return (
        <div className="selfie shadow">
            <img src={imgSelfie} alt="selfie" />
            <div className="selfie_signet">
                <img src={imgSygnet} alt="signet" />
            </div>
        </div>

    )
}

export default Selfie;