import SkillsList from './skillsList';

const SkillsTable = (props) => {
    return (
        <div className="skills-table">
            <SkillsList />
        </div>
    )
}
export default SkillsTable;