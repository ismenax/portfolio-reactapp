
import OtherTitle from '../otherTitle';
import ArrowLink from '../arrowLink';

const TitleWithLinks = (props) => {
    return (
       
             <div className="blog-categories shadow">
                 <OtherTitle title={props.title}/>
                 <ArrowLink text={props.text} />
             </div>
    )
}

export default TitleWithLinks;