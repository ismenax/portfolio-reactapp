

import TextWithLiksButtons from '../textWithLiksButtons';
import imgElem01 from '../../img/category-elems_elem_01.jpg'
import imgElem02 from '../../img/category-elems_elem_02.jpg'
import imgElem03 from '../../img/category-elems_elem_03.jpg'
import imgElem04 from '../../img/category-elems_elem_04.jpg'
import imgElem05 from '../../img/category-elems_elem_05.jpg'
import imgElem06 from '../../img/category-elems_elem_06.jpg'

const CategoriesDisplay = () => {
    const list =
    {
        elems: [
            {
                'id': 'imgElem01',
                'img': imgElem01,

            },
            {
                'id': 'imgElem02',
                'img': imgElem02,

            },
            {
                'id': 'imgElem03',
                'img': imgElem03,

            },
            {
                'id': 'imgElem04',
                'img': imgElem04,

            },
            {
                'id': 'imgElem05',
                'img': imgElem05,

            },
            {
                'id': 'imgElem06',
                'img': imgElem06,

            }
        ]
    }
    

    return (
        <div className="main-content__wrapper_col col-with-categories">
            <div id="CATEGORY-01" className="category-elems">
                {
                    list.elems.map(item => {
                        return (
                            <div key={item.id} className="category-elems_elem">
                                <a href="/"><img className="shadow" src={item.img} alt="work" /></a>
                                <div className="category-elems_elem_elem-text">
                                    <div className='links'>
                                        <TextWithLiksButtons text={["See more:"]} />
                                    </div></div>
                            </div>
                        )
                    })}
            </div>
            <div id="CATEGORY-02" className="category-elems">
                <p>Category 02 elements</p>
            </div>
            <div id="CATEGORY-03" className="category-elems">
                <p>Category 03 elements</p>
            </div>
            <div id="CATEGORY-04" className="category-elems">
                <p>Category 04 elements</p>
            </div>
        </div>
    )
};
export default CategoriesDisplay;