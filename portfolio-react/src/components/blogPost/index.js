import Text from '../../components/text';

const BlogPost = (props) => {
    return (
    <div className="blog-post">
        <div className="blog-post_img">
            <img className="shadow" src={props.img}
                alt={props.title} />
        </div>
        <div className="blog-post_content">
            <div className="title">
                <div className="title_main-title">
                    <h2>{props.title}</h2>
                </div>
                <div className="title_subtitle">
                    <h3>{props.subtitle}</h3>
                </div>
            </div>
            <div className="blog-post_content_author">
                <p>{props.author}, {props.date}</p>
            </div>
            <div className="title_content-text">
                {
                    <Text text={props.text} />
                }
            </div>
            <div className="links">
                <div className="links-text">
                    <a href="/">
                        <p>Read the article &gt;</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    )
}

export default BlogPost;