const Text = (props) => {
    const text = props.text;
    return (
        <div className="content-text">{
            text.map((value, index) => {
                return (
                    <div key={index}>
                        { (value !== '<br/>') &&
                            <p>{value}</p>
                        }
                        { (value === '<br/>') &&
                            <br />
                        }
                    </div>
                )
            })}
        </div>
    )
}

export default Text;