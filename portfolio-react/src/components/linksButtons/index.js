import imgBitbucket from '../../img/ikona_bitbucket_ciemna.png';
import imgDEV from '../../img/ikona_DEV_ciemna.png';
import { Link } from "react-router-dom";

const LinksButtons = (props) => {
    return (
        <div className="links-buttons">
            <Link to="."><img className="shadow" src={imgBitbucket} alt="Bitbucket" /></Link>
            <Link to=""><img className="shadow" src={imgDEV} alt="DEV" /></Link>
        </div>
    )
}

export default LinksButtons;