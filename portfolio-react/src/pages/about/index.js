
import components from '../../components'

const AboutPage = () => {
    const { Menu, MainTitle, OtherTitle, Text, ToolTable, TextWithLinksButtons, MyInterests, SkilsTable, GraphElems } = components;
    return (
        <div className="main-wrapper" id="about">
            <Menu />
            <div className="main-content">
                <div className="main-content__wrapper">
                    <div className="main-content__wrapper_col-left-75" >
                        <div className="main-content__wrapper_col-left_about-me shadow">
                            <MainTitle sygnet={true} title={'About me'} subtitle={'All about Tech'} />
                            <Text text={['<br/>', '<br/>', 'Lorem ipsum dolor sit amet,', '<br/>', 'consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '<br/>', '<br/>']} />
                            <TextWithLinksButtons text={'See my works on Bitbucket and DEV  '} />
                        </div>
                        <div className='main-content__wrapper_col-left_my-interests'>
                            <MyInterests subtitle={'My interests'} text={['<br/>', 'music', 'jogging', 'mountain climbing', 'art']} />
                        </div>
                    </div>
                    <div className="main-content__wrapper_col-right_my-skils">
                        <OtherTitle sygnet={true} title={'My skills'} subtitle={'Secondary title'} />
                        <Text text={['<br/>', 'Dauis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex', 'eacommodo consequat.']} />
                        <SkilsTable />
                    </div>
                    <hr className="width-100"></hr>
                    <div className="main-content__wrapper_col col-with-graph">
                        <div className="main-content__wrapper_col-narrow">
                            <OtherTitle sygnet={true} title={'Tools'} subtitle={'Programms I use'} />
                            <Text text={['<br/>', 'Essentials to make the work done.']} />
                        </div>
                        <div className="main-content__wrapper_col-wide">
                            <ToolTable />
                        </div>
                    </div>
                </div>
                <GraphElems id={'about'} />
            </div>
        </div >
    )
};
export default AboutPage;
