import ToolsList from './toolsList';
const ToolTable = (props) => {

    return (
        <div className="tools-table shadow">
            <ToolsList />
        </div>
    )
}
export default ToolTable;