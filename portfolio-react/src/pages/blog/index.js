
import components from '../../components';
import imgSelfie from '../../img/portfolio_programista_sidebar-21.png';
import imgBlog2 from '../../img/portfolio_programista_sidebar-20.png';
import imgBlog1 from '../../img/portfolio_programista_sidebar-22.png';

const BlogPage = () => {
    const { Menu, TitleWithLinks, MainTitle, HashTags, Text,BlogPost, GraphElems } = components;
    return (
        <div className="main-wrapper" id="blog">
            <Menu />
            <div className="main-content">
                <div className="main-content__wrapper">
                    <div className="main-content__wrapper_col">
                        <div className="main-content__wrapper_col-wide">
                            <MainTitle sygnet={true} title={'Blog'} subtitle={'My subjective point of programming'} />
                            <Text text={['Lorem ipsum, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.']} />
                        </div>
                        <div className="main-content__wrapper_col-narrow">
                            <img className="shadow" src={imgSelfie} alt="selfie" />
                        </div>
                    </div>
                    <div className="main-content__wrapper_col">
                        <div className="main-content__wrapper_col-wide">
                            <BlogPost img={imgBlog1}vtitle={'Blog post 01'} subtitle={'Secondary title'} author={'author'} date={'12/09/2020'} text={['Lorem ipsum, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.']}/>
                            <BlogPost img={imgBlog2} title={'Blog post 02'} subtitle={'Secondary title'} author={'author'} date={'12/09/2020'} text={['Lorem ipsum, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.']}/>
                        </div>
                        <div className="main-content__wrapper_col-narrow">
                            <TitleWithLinks title="Categories" text={['Latest', 'Theme 01', 'Tips & Tricks', 'Off top', 'FAQ on freelance programming']} />
                       <HashTags title={'Read about'} text={['#freelance', '#online courses', '#networking', '#home office', '#programming', '#freelance', '#blog']}/>
                        </div>
                    </div>
                </div>
                <GraphElems id={'blog'} />
            </div>
        </div >
    )
};
export default BlogPage;
