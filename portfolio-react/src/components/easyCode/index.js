import imgEasyCode from '../../img/easy_code_button_grey.png';
import { Link } from "react-router-dom";

const EasyCode = (props) => {
    return (
        <div className="easy-code shadow">
            <p>Ukonczyłam kurs Easy Code</p>
            <div className="easy-code_button shadow">
                <Link to="https://easy-code.io/"><img src={imgEasyCode}
                    alt="easy-code" /></Link>
            </div>
        </div>
    )
}

export default EasyCode;