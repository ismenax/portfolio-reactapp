import './App.css';
import React from "react";
import HomePage from './pages/home';
import AboutPage from './pages/about';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import PortfolioPage from './pages/portfolio';
import ContactPage from './pages/contact';
import BlogPage from './pages/blog';
function App() {
  return (
    <Router>
      <Switch>
          <Route exact path='/' component={HomePage} />
          <Route path='/home' component={HomePage} />
          <Route path='/about' component={AboutPage} />     
          <Route path='/portfolio' component={PortfolioPage} />          
          <Route path='/contact' component={ContactPage} />  
          <Route path='/blog' component={BlogPage} />  
        </Switch>
    </Router>
  );
}

export default App;
