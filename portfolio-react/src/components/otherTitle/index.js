import imgSygnet from '../../img/sygnet_kwadraty_.png';
const OtherTitle = (props) => {
    return (
        <div className="title">
            { props.sygnet &&
                <div className="title_signet"><img className="shadow" src={imgSygnet} alt="."></img></div>}
            { props.title &&
                <div className="title_main-title"><h2>{props.title}</h2></div>}
            {props.subtitle &&
                <div className="title_subtitle">
                    <h3>{props.subtitle}</h3></div>}
        </div>

    )
}

export default OtherTitle;