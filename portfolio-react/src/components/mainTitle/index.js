import imgSygnet from '../../img/sygnet_kwadraty_.png';
const MainTitle = (props) => {
    return (
        <div className="title">
            { props.sygnet &&
                <div className="title_signet"><img className="shadow" src={imgSygnet} alt="."></img></div>}
            { props.title &&
                <div className="title_main-title"><h1>{props.title}</h1></div>}
            {props.subtitle &&
                <div className="title_subtitle">
                    <h2>{props.subtitle}</h2></div>}
        </div>

    )
}

export default MainTitle;