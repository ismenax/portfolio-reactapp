
import components from '../../components'
import imgGraphElem2 from '../../img/element_graficzny_tła_02.png'

const HomePage = () => {
    const { Menu, MainTitle, OtherTitle, Text, LinksButtons, EasyCode, Selfie, LinksButtonsCV, GraphElems } = components;
    return (
        <div className="main-wrapper" id="home">
            <Menu/>
            <div className="main-content">
                <div className="main-content__wrapper">
                    <div className="main-content__wrapper_col-left col-left-with-margin">
                        <MainTitle sygnet={true} title={'Hi! My name is Jane Doe'} subtitle={'Passionate software engineer'} />
                        <Text text={["Passionate Techy and Tech Author with 5 years",  "of valuable experience within the field of software engineering.",  "<br/>", "See my works on Bitbucket and DEV"]} />
                        <LinksButtons />
                        <hr />
                        <div className="main-content__wrapper_col-left_graph-elem">
                            <img src={imgGraphElem2} alt="" />
                        </div>
                        <Text text={["autor: Jane Doe",  "username: @janedoe",  "description: University Graduate | Software Engineer",  "homepage: janedoe.bitbucket.pl",  "repository type: Open-source",  "url: bitbucket.com/janedoe"]} />
                        <EasyCode />
                    </div>
                    <div className="main-content__wrapper_col-right">
                        <Selfie />
                        <OtherTitle sygnet={true} title={'I am a freelancer'} subtitle={'So you can ask me to join your project'} />
                        <Text text={["You can ask me to join the conference or hire me to work with you on some project. Please,",  "send me a short brief or contact me via e-mail. I will be happy to help you"]} />
                        <LinksButtonsCV />
                    </div>
                </div>
                <GraphElems id={'home'}/>
            </div>
        </div>
    )
};
export default HomePage;
