import MainTitle from './mainTitle';
import OtherTitle from './otherTitle';
import Text from './text';
import LinksButtons from './linksButtons';
import EasyCode from './easyCode';
import Selfie from './selfie';
import LinksButtonsCV from './linksButtonsCV';
import GraphElems from './grapfElems'
import Menu from './menu';
import TextWithLinksButtons from './textWithLiksButtons';
import MyInterests from './myInterests';
import SkilsTable from './skillsTable';
import ToolTable from './tootlTable';
import Categories from './categories';
import CategoriesDisplay from './categoriesDisplay';
import ContactForm from './contactForm';
import TitleWithLinks from './titleWithLinks';
import Partners from './partners';
import HashTags from './hashTags';
import BlogPost from './blogPost';
const components = {
    MainTitle,
    OtherTitle,
    Text,
    LinksButtons,
    EasyCode,
    Selfie,
    LinksButtonsCV,
    GraphElems,
    Menu,
    TextWithLinksButtons,
    MyInterests,
    SkilsTable,
    ToolTable,
    Categories,
    CategoriesDisplay,
    ContactForm,
    TitleWithLinks,
    Partners,
    HashTags,
    BlogPost

}

export default components;