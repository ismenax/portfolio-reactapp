import imgLine from '../../img/_percentage_line_100.png';
import React from 'react';
const SkillsList = () => {
    const list =
    {
        skills: [
            {
                'percent': '80%',
                'skill': 'PHP',
                'id': '1'
            },
            {
                'percent': '100%',
                'skill': 'JS',
                'id': '2'
            },
            {
                'percent': '100%',
                'skill': 'HTML',
                'id': '3'
            },
            {
                'percent': '60%',
                'skill': 'NODEJS',
                'id': '4'
            },
            {
                'percent': '100%',
                'skill': 'CSS',
                'id': '5'
            },
            {
                'percent': '80%',
                'skill': 'GO',
                'id': '6'
            },
            {
                'percent': '100%',
                'skill': 'NEXT',
                'id': '7'
            }

        ]
    }

    return (
        list.skills.map(item => {
            return (<React.Fragment key={item.id}>
                <div className="skills-table_elem">
                    <div className="percent">{item.percent}</div>
                    <div className="skill">{item.skill}</div>
                </div>
                <div className="skills-table_elem shadow">
                    <img src={imgLine} alt="percentage_line" />
                </div>
            </React.Fragment>
            )
        })
    )
}
export default SkillsList;