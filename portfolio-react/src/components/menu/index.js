import React from 'react';
import MenuList from './menuList';

const Menu = () => {
    return (
        <div className="main-nav">
            <ul className="main-nav__wrapper shadow">
                <MenuList />
            </ul>
        </div>
    )
}

export default Menu;