
import { useEffect } from 'react';

function openCategory(categoryName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("category-elems");
    for (i of tabcontent) i.style.display = "none";
    tablinks = document.getElementsByClassName("categories-tab_link");
    for (i of tablinks) i.className = i.className.replace(" active", "");
    document.getElementById(categoryName).style.display = "flex";
    document.getElementById(categoryName).className += " active";
}


const Categories = () => {
    useEffect(() => {
        document.getElementById("defaultOpen").click();
    });

    return (
        <div className="categories">
            <div className="categories-tab">
                <button id="defaultOpen" className="categories-tab_link"
                    onClick={() => openCategory('CATEGORY-01')}>CATEGORY 01</button>
                <button className="categories-tab_link"
                    onClick={() => openCategory('CATEGORY-02')}>CATEGORY 02</button>
                <button className="categories-tab_link"
                    onClick={() => openCategory('CATEGORY-03')}>CATEGORY 03</button>
                <button className="categories-tab_link"
                    onClick={() => openCategory('CATEGORY-04')}>CATEGORY 04</button>
            </div>
        </div>
    )
};
export default Categories;