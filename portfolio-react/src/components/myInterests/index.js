import OtherTitle from '../otherTitle'
import Text from '../text';

const MyInterests = (props) => {

    return (
        <div>
            <OtherTitle subtitle={[props.subtitle]} />
            <Text text={props.text} />
        </div>
    )
}
export default MyInterests;