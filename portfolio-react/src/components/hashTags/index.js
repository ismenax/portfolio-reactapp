import React from "react";
import OtherTitle from "../otherTitle";

const HashTags = (props) => {
    const text = props.text;
    return (
        <div className="blog-read-about">
            <div className="blog-read-about_title">
                {<OtherTitle title={props.title} />}
            </div>
            <div className="blog-read-about_content">
                {
                    text.map((value, index) => {
                        return (
                            <React.Fragment key={index}>
                                <div className="blog-read-about_content_elem">
                                    <a href="/">{value}</a>
                                </div>
                            </React.Fragment>
                        )
                    })
                }
            </div>
        </div>
    )
}
export default HashTags;