import img1 from '../../img/element_graficzny_tła_01.png';
import img2 from '../../img/element_graficzny_tła_02.png';
import img3 from '../../img/element_graficzny_tła_03_pionowy.png';
import img4 from '../../img/element_graficzny_tła_04_pionowy.png';

const GraphElems = (props) => {
    return (<div>
        {(props.id === 'home') &&
            <div className="graph-elems">
                <div className="graph-elems_elem-1">
                    <img src={img1} alt="Graph elem 1" />
                </div>
                <div className="graph-elems_elem-3">
                    <img src={img3} alt="Graph elem 3" />
                </div>
                <div className="graph-elems_elem-4">
                    <img src={img4} alt="Graph elem 4" />
                </div>
            </div>}
        {(props.id === 'about') &&
            <div className="graph-elems">
                <div className="graph-elems_elem-1">
                    <img src={img1} alt="Graph elem " />
                </div>
                <div className="graph-elems_elem-2">
                    <img src={img2} alt="Graph elem " />
                </div>
                <div className="graph-elems_elem-3">
                    <img src={img3} alt="Graph elem " />
                </div>
            </div>}
        {(props.id === 'portfolio') &&
            <div className="graph-elems">
                <div className="graph-elems_elem-1">
                    <img src={img1} alt="Graph elem 1" />
                </div>
                <div className="graph-elems_elem-2">
                    <img src={img2} alt="Graph elem 2" />
                </div>
                <div className="graph-elems_elem-4">
                    <img src={img4} alt="Graph elem 4" />
                </div>
            </div>}
        {(props.id === 'blog') &&
            <div className="graph-elems">
                <div className="graph-elems_elem-1">
                    <img src={img1} alt="Graph elem 1" />
                </div>
                <div className="graph-elems_elem-2">
                    <img src={img2} alt="Graph elem 2" />
                </div>
                <div className="graph-elems_elem-3">
                    <img src={img3} alt="Graph elem 3" />
                </div>
            </div>}
        {(props.id === 'contact') &&
            <div className="graph-elems">
                <div className="graph-elems_elem-1">
                    <img src={img1} alt="Graph elem 1" />
                </div>
                <div className="graph-elems_elem-3">
                    <img src={img3} alt="Graph elem 3" />
                </div>
                <div className="graph-elems_elem-4">
                    <img src={img4} alt="Graph elem 4" />
                </div>
            </div>}
    </div>
    )
}

export default GraphElems;