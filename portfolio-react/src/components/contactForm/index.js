import MainTitle from '../mainTitle';
import Text from '../text';
const ContactForm = () => {


    return (
        <div className="contact-form shadow">
            <MainTitle sygnet='true' title='Contact me' />
            <Text text={['If you are willing to work with me, contact me. I can join your conference to serve you with my engeneering experience.']} />

            <form className="form" action="">
                <label htmlFor="fname">Please, enter yout name</label>
                <input type="text" id="fname" name="firstname" placeholder="" />
                <label htmlFor="lname">E-mail address</label>
                <input type="text" id="lname" name="lastname" placeholder="" />

                <label htmlFor="message">Your message</label>
                <input type="text" id="message" name="message" placeholder="" />
                <p><br /></p>
                <div className="form_send-button"><input type="submit" value="Send" /></div>
            </form>
        </div>
    )
};
export default ContactForm;