import OtherTitle from '../otherTitle';
const Partners = (props) => {
    return (
        <div className="partners">
            <OtherTitle sygnet='true' title='Partners' />
            <div className="partners_list">
                <div className="partners_list_elem">
                    <a href="/">
                        <div className="partners_list_elem_text">EasyCode</div>
                        <div className="partners_list_elem_sign">&gt;</div>
                    </a>
                </div>
                <div className="partners_list_elem">
                    <a href="/">
                        <div className="partners_list_elem_text">TECHOOO Techy Vlog 01</div>
                        <div className="partners_list_elem_sign">&gt;</div>
                    </a>
                </div>
            </div>
        </div>
    )
}

export default Partners;