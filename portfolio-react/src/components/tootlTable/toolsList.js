import imgReact from '../../img/_react_icon_s.png';
import imgWebpack from '../../img/webpack_icon.png';
import imgExpress from '../../img/express_icon.png';
import imgStyledComponents from '../../img/styled_components_icon.png';
import imgRedux from '../../img/redux_icon_s.png';
import imgFlexbox from '../../img/flexbox_icon_s.png';
import imgProgram from '../../img/other_tool_programm_icon_sygnet.png';

const ToolsList = () => {
    const list =
    {
        tools: [
            {
                'tool': 'React',
                'version': '16.6.3',
                'id': '1',
                'img': imgReact
            },
            {
                'tool': 'Webpack',
                'version': '4.19.1',
                'id': '2',
                'img': imgWebpack
            },
            {
                'tool': 'Express',
                'version': '4.16.4',
                'id': '3',
                'img': imgExpress
            },
            {
                'tool': 'Styled Components',
                'version': '4.16.4',
                'id': '4',
                'img': imgStyledComponents
            },
            {
                'tool': 'Redux',
                'version': '4.0.1',
                'id': '5',
                'img': imgRedux
            },
            {
                'tool': 'Flexbox',
                'version': '4.0.1',
                'id': '6',
                'img': imgFlexbox
            },
            {
                'tool': 'Program',
                'version': '5.2.1',
                'id': '7',
                'img': imgProgram
            }
            ,
            {
                'tool': 'Program',
                'version': '5.2.2',
                'id': '8',
                'img': imgProgram
            }

        ]
    }

    return (
        list.tools.map(item => {
            return (
                <div key={item.id} className="tools-table_elem shadow">
                    <img src={item.img} alt={item.tool} />
                    <p>{item.tool}<br />{item.version}</p>
                </div>
            )
        })
    )
}
export default ToolsList;