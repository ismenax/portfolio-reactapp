
import components from '../../components';
import React from "react";
import imgSelfie from '../../img/portfolio_programista_sidebar-10.png';


const PortfolioPage = () => {
    const { Menu, MainTitle, Text, Categories, CategoriesDisplay, GraphElems } = components;
    return (

        <div className="main-wrapper" id="portfolio">
            <Menu />
            <div className="main-content">
                <div className="main-content__wrapper">
                    <div className="main-content__wrapper_col">
                        <div className="main-content__wrapper_col-wide">
                            <MainTitle sygnet={true} title={'Portfolio'} subtitle={'See the works I enjoyed working on'} />
                            <Text text={['Lorem ipsum, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.']} />
                            <Categories />
                        </div>
                        <div className="main-content__wrapper_col-narrow">
                            <img className="shadow" src={imgSelfie} alt="selfie" />
                        </div>
                    </div>
                    <CategoriesDisplay />
                </div>
                <GraphElems id={'portfolio'} />
            </div>
        </div >
    )
};
export default PortfolioPage;
