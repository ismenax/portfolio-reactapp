
import components from '../../components'
import imgRead from '../../img/portfolio_programista_sidebar-22.png'

const ContactPage = () => {
    const { ContactForm, TitleWithLinks, Partners, Menu, Text, GraphElems } = components;
    return (
        <div className="main-wrapper" id="contact">
            <Menu />
            <div className="main-content">
                <div className="main-content__wrapper">
                    <div className="main-content__wrapper_col-left col-left-with-margin">
                        <ContactForm />
                        <Text text={["autor: Jane Doe", "username: @janedoe", "description: University Graduate | Software Engineer", "homepage: janedoe.bitbucket.pl", "repository type: Open-source", "url: bitbucket.com/janedoe"]} />
                    </div>
                    <div className="main-content__wrapper_col-narrow">
                        <div className="blog-post_img">
                            <img className="shadow" src={imgRead} alt="new-blog-post" />
                        </div>
                        <div className="blog-post_read">
                            <TitleWithLinks title="Read" text={['Blog Post 01', 'Blog Post 02', 'Blog Post 03']} />
                        </div>
                        <Partners />
                    </div>
                </div>

            </div>
            <GraphElems id={'contact'} />
        </div>
    )
};
export default ContactPage;
