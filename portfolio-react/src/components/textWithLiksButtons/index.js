import LinksButtons from '../linksButtons'
import Text from '../text';

const TextWithLiksButtons = (props) => {

    return (
        <div className='text-with-links-buttons'>
            <Text text={[props.text]} />
            <LinksButtons />
        </div>
    )
}
export default TextWithLiksButtons;